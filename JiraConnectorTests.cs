﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using JiraConnector.Models;
using System.Collections.Generic;

namespace JiraConnectorTests
{
    [TestClass]
    public class UnitTest1
    {
        private TestConfiguration testConfiguration = new TestConfiguration();

        [TestMethod]
        public void GetAllProjectsTest()
        {
            

            var jiraConnector = new JiraConnector.JiraConnector("tassilok@gmx.de", "oJ_4A2w/9j$Mz}K");

            var taskResult = Task.Run<GetProjectsResponse>(() =>
            {
                return jiraConnector.GetProjects(new JiraConnector.Models.GetProjectsRequest { AllProjects = true, RestUrl = testConfiguration.Configuration.RestUrl });
            });

            taskResult.Wait();


            
        }

        [TestMethod]
        public void GetAllIssuesTest()
        {
            var jiraConnector = new JiraConnector.JiraConnector("tassilok@gmx.de", "oJ_4A2w/9j$Mz}K");

            var taskResultProjects = Task.Run<GetProjectsResponse>(() =>
            {
                return jiraConnector.GetProjects(new JiraConnector.Models.GetProjectsRequest { AllProjects = true, RestUrl = testConfiguration.Configuration.RestUrl });
            });

            taskResultProjects.Wait();

            var issues = new List<Issue>();
            foreach (var jiraProject in taskResultProjects.Result.JiraProjects)
            {
                var taskResultIssues = Task.Run<GetIssuesResponse>(() =>
                {
                    return jiraConnector.GetIssues(new JiraConnector.Models.GetIssuesRequest(
                        restUrl: testConfiguration.Configuration.RestUrl, 
                        projectKey: jiraProject.Key, 
                        startAt: "0", 
                        maxResults: "-1"));
                });
                taskResultIssues.Wait();

                issues.AddRange(taskResultIssues.Result.Issues);
            }

        }
    }
}
