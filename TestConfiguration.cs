﻿using JiraConnector.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnectorTests
{
    public class TestConfiguration
    {
        public AppConfiguration Configuration => new AppConfiguration { RestUrl = "https://omodules.atlassian.net/rest/api/2/" };
            
    }
}
